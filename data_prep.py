import time
import sys
import pandas as pd

basetable = pd.read_csv('basetable.csv')

print(basetable.info())
# Assign the number of rows in the basetable to the variable 'population_size'.
population_size  = len(basetable)

# Print the population size.
print(population_size)

# Assign the number of targets to the variable 'targets_count'.
targets_count = sum(basetable['target'])

# Print the number of targets.
print(targets_count)

# Print the target incidence.
print(targets_count / population_size)

print('doidejfd')
basetable['gender'] = basetable['gender_F'].apply(lambda x: 'F' if x == 1 else 'M')

print(basetable.head(100))

# Count and print the number of females.
print(sum(basetable['gender'] =='F'))

# Count and print the number of males.
print(sum(basetable['gender'] =='M'))