# Import linear_model from sklearn.
from sklearn import linear_model
from sklearn.metrics import roc_auc_score
import time
import sys
import pandas as pd

basetable = pd.read_csv('basetable.csv')

print(basetable.info())

# Construct a logistic regression model that predicts the target using age, gender_F and time_since_last gift
predictors = ["age","gender_F","time_since_last_gift"]
X = basetable[predictors]

# Create a dataframe y that contains the target.
#y = basetable[['target']]
y = basetable.loc[:,'target'].values

# Create a logistic regression model logreg and fit it to the data.
logreg = linear_model.LogisticRegression(solver='lbfgs')
logreg.fit(X,y)

# Assign the coefficients to a list coef
coef = logreg.coef_

for p, c in zip(predictors, list(coef[0])):
    print(p + '\t' + str(c))

# Assign the intercept to the variable intercept
intercept = logreg.intercept_
print(intercept)

# Make predictions
predictions = logreg.predict_proba(X)
predictions_target = predictions[:,1]

# Calculate the AUC value
auc = roc_auc_score(y,predictions_target)
print('what is the roc score: {0:2f}'.format(round(auc,2)))

variables_1=['mean_gift', 'income_low']
variables_2=['mean_gift', 'income_low', 'gender_F', 'country_India', 'age']

# Create appropriate dataframes
print(variables_1)
print(variables_2)
X_1 = basetable[variables_1]
X_2 = basetable[variables_2]
y = basetable.loc[:,'target'].values

print(X_1)
print(X_2)
# Create the logistic regression model
logreg = linear_model.LogisticRegression(solver='lbfgs')

# Make predictions using the first set of variables and assign the AUC to auc_1
logreg.fit(X_1, y)
predictions_1 = logreg.predict_proba(X_1)[:,:]
auc_1 = roc_auc_score(y, predictions_1)

# Make predictions using the second set of variables and assign the AUC to auc_2
logreg.fit(X_2, y)
predictions_2 = logreg.predict_proba(X_2)[:,1]
auc_2 = roc_auc_score(y,predictions_2)

# Print auc_1 and auc_2
print(round(auc_1,2))
print(round(auc_2,2))

