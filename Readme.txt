Machine Learning - Logistic Regression Binary Classification (Datacamp.com)

Whether a person will make a donation or not

For general queries and support, please contact Ozan Senturk
(ozan.senturk@gmail.com)

Yours truly,

Ozan Senturk

p.s: This code is implemented by Datacamp.com
https://www.datacamp.com/courses/foundations-of-predictive-analytics-in-python-part-1

