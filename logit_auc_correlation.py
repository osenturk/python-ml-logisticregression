from sklearn import linear_model
from sklearn.metrics import roc_auc_score
# Import the matplotlib.pyplot module
import matplotlib.pyplot as plt

# Import the scikitplot module
import scikitplot as skplt

def auc(variables, target, basetable):
    X = basetable[variables]
    y = basetable.loc[:, target].values
    #y = np.array(target)

    # Create the logistic regression model
    logreg = linear_model.LogisticRegression(solver='lbfgs')

    # Make predictions using the first set of variables and assign the AUC to auc_1
    logreg.fit(X, y)
    predictions = logreg.predict_proba(X)[:, :]
    #auc = roc_auc_score(y, predictions)

    # Plot the cumulative gains graph
    skplt.metrics.plot_cumulative_gain(y, predictions)
    plt.show()

    return  auc


import numpy as np
import pandas as pd

basetable = pd.read_csv('basetable.csv')

# Calculate the AUC of the model using min_gift only
auc_min_gift = auc(['min_gift'], "target", basetable)
print(round(auc_min_gift,2))

# Calculate the AUC of the model using income_high only
auc_income_high = auc(['income_high'], "target", basetable)
print(round(auc_income_high,2))

# Calculate the correlation between min_gift and mean_gift
correlation = np.corrcoef(basetable["min_gift"], basetable["mean_gift"])[0,1]
print(round(correlation,2))