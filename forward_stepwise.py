from sklearn import linear_model
from sklearn.metrics import roc_auc_score

def auc(variables, target, basetable):
    X = basetable[variables]
    y = basetable.loc[:, target].values

    # Create the logistic regression model
    logreg = linear_model.LogisticRegression(solver='lbfgs')

    # Make predictions using the first set of variables and assign the AUC to auc_1
    logreg.fit(X, y)
    predictions = logreg.predict_proba(X)[:, 1]
    auc = roc_auc_score(y, predictions)

    return  auc

def next_best(current_variables, candidate_variables, target, basetable):

    best_auc = -1
    best_variable = None

    y = basetable.loc[:, target].values

    for v in candidate_variables:
        temp_variables=current_variables + [v]
        auc_v =auc(temp_variables, target, basetable)

        if auc_v >= best_auc:
            best_auc = auc_v
            best_variable = v

    return  best_variable



import pandas as pd

basetable = pd.read_csv('basetable.csv')

#auc = auc(["age","gender_F","min_gift"],"target",basetable)
#print(round(auc,2))

current_variables =["age","gender_F"]
candidate_variables = ["min_gift","max_gift","mean_gift"]

#target = basetable.loc[:,'target'].values
basetable = pd.read_csv('basetable.csv')

next_variable = next_best(current_variables,candidate_variables,"target",basetable)
print(next_variable)


########################
basetable = pd.read_csv('basetable.csv')

# Calculate the AUC of a model that uses "max_gift", "mean_gift" and "min_gift" as predictors
auc_current = auc(['max_gift','mean_gift', 'min_gift'], "target", basetable)
print(round(auc_current,4))


# Calculate which variable among "age" and "gender_F" should be added to the variables "max_gift", "mean_gift" and "min_gift"
next_variable = next_best(['max_gift','mean_gift', 'min_gift'], ['age', 'gender_F'], "target", basetable)
print(next_variable)

# Calculate the AUC of a model that uses "max_gift", "mean_gift", "min_gift" and "age" as predictors
auc_current_age = auc(['max_gift','mean_gift', 'min_gift','age'], "target", basetable)
print(round(auc_current_age,4))

# Calculate the AUC of a model that uses "max_gift", "mean_gift", "min_gift" and "gender_F" as predictors
auc_current_gender_F =auc(['max_gift','mean_gift', 'min_gift','gender_F'], "target", basetable)
print(round(auc_current_gender_F,4))

########################

# Find the candidate variables
candidate_variables = list(basetable.columns.values)
candidate_variables.remove("target")

# Initialize the current variables
current_variables = []

# The forward stepwise variable selection procedure
number_iterations = 5
for i in range(0, number_iterations):
    next_variable = next_best(current_variables, candidate_variables, "target", basetable)
    current_variables = current_variables + [next_variable]
    candidate_variables.remove(next_variable)
    print("Variable added in step " + str(i+1)  + " is " + next_variable + ".")
print(current_variables)