from sklearn import linear_model
from sklearn.metrics import roc_auc_score
# Import the matplotlib.pyplot module
import matplotlib.pyplot as plt

# Import the scikitplot module
import scikitplot as skplt

import numpy as np
import pandas as pd

basetable = pd.read_csv('basetable.csv')


X = basetable[["age","gender_F"]]
y = basetable.loc[:, "target"].values
# y = np.array(target)

# Create the logistic regression model
logreg = linear_model.LogisticRegression(solver='lbfgs')

# Make predictions using the first set of variables and assign the AUC to auc_1
logreg.fit(X, y)
predictions = logreg.predict_proba(X)[:, :]
# auc = roc_auc_score(y, predictions)

# Plot the cumulative gains graph
skplt.metrics.plot_cumulative_gain(y, predictions)
plt.show()
