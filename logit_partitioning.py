# Load the partitioning module
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import roc_auc_score

import pandas as pd

def auc(variables, target, basetable):
    X = basetable[variables]
    y = basetable.loc[:, target].values

    # Create the logistic regression model
    logreg = linear_model.LogisticRegression(solver='lbfgs',max_iter=10)

    # Make predictions using the first set of variables and assign the AUC to auc_1
    logreg.fit(X, y)
    predictions = logreg.predict_proba(X)[:, 1]
    auc = roc_auc_score(y, predictions)

    return  auc

def auc_train_test(variables, target, train, test):

    auc_train=auc(variables, target, train)

    auc_test = auc(variables, target, test)

    return  auc_train, auc_test

basetable = pd.read_csv('basetable.csv')

# Create dataframes with variables and target
X = basetable.drop("target", 1)
y = basetable["target"]

# Carry out 50-50 partititioning with stratification
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.5, stratify = y)

# Create the final train and test basetables
train = pd.concat([X_train, y_train], axis=1)
test = pd.concat([X_test, y_test], axis=1)

# Check whether train and test have same percentage targets
print(round(sum(train['target'])/len(train), 2))
print(round(sum(test['target'])/len(test), 2))

# Create dataframes with variables and target
X = basetable.drop('target', 1)
y = basetable["target"]

# Carry out 70-30 partititioning with stratification
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size =0.3, stratify = y)

# Create the final train and test basetables
train = pd.concat([X_train, y_train], axis=1)
test = pd.concat([X_test, y_test], axis=1)

 # Apply the auc_train_test function
auc_train, auc_test = auc_train_test(['age', 'gender_F'], ["target"], train, test)
print(round(auc_train,2))
print(round(auc_test,2))

######################################

variables = ['max_gift', 'time_since_last_gift', 'number_gift', 'mean_gift', 'income_high', 'age', 'gender_F', 'time_since_first_gift', 'income_low', 'country_UK']

# Keep track of train and test AUC values
auc_values_train = []
auc_values_test = []
variables_evaluate = []

print(variables)

# Iterate over the variables in variables
for v in variables:
    # Add the variable
    variables_evaluate.append(v)

    # Calculate the train and test AUC of this set of variables
    auc_train, auc_test = auc_train_test(variables_evaluate, "target", train, test)

    # Append the values to the lists
    auc_values_train.append(auc_train)
    auc_values_test.append(auc_test)

# Make plot of the AUC values
import matplotlib.pyplot as plt
import numpy as np

x = np.array(range(0, len(auc_values_train)))
y_train = np.array(auc_values_train)
y_test = np.array(auc_values_test)
plt.xticks(x, variables, rotation=90)
plt.plot(x, y_train)
plt.plot(x, y_test)
plt.ylim((0.6, 0.8))
plt.show()